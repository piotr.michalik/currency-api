﻿using System;
using System.Collections.Generic;
using System.Linq;
using CurrencyAPI.Models;

namespace CurrencyAPI.Tests
{
    public class FakeLogRepository : ILogRepository
    {
        private List<Log> _context = new List<Log>();

        public void SaveLog(DateTime RequestedAt, long RequestTime, int StatusCode, string Method, string Path, string RequestBodyContent, string ResponseBodyContent)
        {
            _context.Add(new Log(RequestedAt, RequestTime, StatusCode, Method, Path, RequestBodyContent, ResponseBodyContent));
        }
        public IQueryable<Log> Logs => _context.AsQueryable();
    }
}
