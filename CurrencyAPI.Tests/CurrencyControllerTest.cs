﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using CurrencyAPI.Controllers;
using CurrencyAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace CurrencyAPI.Tests
{
    public class CurrencyControllerTest
    {
        CurrencyController _controller;
        ILogRepository _logRepository;

        public CurrencyControllerTest()
        {
            _logRepository = new FakeLogRepository();
            _controller = new CurrencyController(_logRepository);
        }

        [Fact]
        public void Get_ReturnsOkResult()
        {
            var okResult = _controller.Get();
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Theory]
        [InlineData("EUR", "USD", 100)]
        [InlineData("USD", "THB", 123.56)]
        public void Exchange_ReturnsSame(string fromCurrencyCode, string toCurrencyCode, decimal amount)
        {
            var exchangeFrom = _controller.Exchange(fromCurrencyCode, toCurrencyCode, amount);
            var exchangeTo = _controller.Exchange(toCurrencyCode, fromCurrencyCode, exchangeFrom.Value.ExchangeValue);
            Assert.Equal(amount, exchangeTo.Value.ExchangeValue);
        }

        [Theory]
        [MemberData(nameof(MultipleCodesData))]
        public void Get_List_ReturnsOkResult(List<string> codes)
        {
            var okResult = _controller.Get(codes);
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        public static IEnumerable<object[]> MultipleCodesData =>
        new List<object[]>
        {
            new object[] { new List<string> { "EUR", "USD", "THB", "EUR" } }
        };
    }
}
