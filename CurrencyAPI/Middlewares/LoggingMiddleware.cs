﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using CurrencyAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;

namespace CurrencyAPI.Middlewares
{
    /// <summary>
    /// Middleware which creates <see cref="Log"/> of every <see cref="HttpRequest"/> and <see cref="HttpResponse"/>
    /// </summary>
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogRepository _logRepository;

        public LoggingMiddleware(RequestDelegate next, ILogRepository logRepository)
        {
            _next = next;
            _logRepository = logRepository;
        }
        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                HttpRequest request = httpContext.Request;
                if (request.Path.StartsWithSegments(new PathString("/api")))
                {
                    Stopwatch stopWatch = Stopwatch.StartNew();
                    DateTime requestTime = DateTime.UtcNow;
                    string requestBodyContent = await ReadRequestBody(request);
                    Stream originalBodyStream = httpContext.Response.Body;
                    using (MemoryStream responseBody = new MemoryStream())
                    {
                        HttpResponse response = httpContext.Response;
                        response.Body = responseBody;
                        await _next(httpContext);
                        stopWatch.Stop();
                        string responseBodyContent = null;
                        responseBodyContent = await ReadResponseBody(response);
                        await responseBody.CopyToAsync(originalBodyStream);
                        _logRepository.SaveLog(requestTime,
                            stopWatch.ElapsedMilliseconds,
                            response.StatusCode,
                            request.Method,
                            request.Path.Value,
                            requestBodyContent,
                            responseBodyContent);
                    }
                }
                else
                {
                    await _next(httpContext);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                await _next(httpContext);
            }
        }

        private async Task<string> ReadRequestBody(HttpRequest request)
        {
            request.EnableRewind();

            byte[] buffer = new byte[Convert.ToInt32(request.ContentLength)];
            await request.Body.ReadAsync(buffer, 0, buffer.Length);
            string bodyAsText = Encoding.UTF8.GetString(buffer);
            request.Body.Seek(0, SeekOrigin.Begin);

            return bodyAsText;
        }

        private async Task<string> ReadResponseBody(HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            string bodyAsText = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);

            return bodyAsText;
        }
    }
}
