﻿using System;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace CurrencyAPI.Models
{
    /// <summary>
    /// Details of request and response
    /// </summary>
    public class Log
    {
        #region Properties
        /// <summary>
        /// Identifier (used as Primary Key)
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Date of request
        /// </summary>
        public DateTime RequestedAt { get; set; }
        /// <summary>
        /// Request durations measured in miliseconds
        /// </summary>
        public long RequestTime { get; set; }
        /// <summary>
        /// Response status code
        /// </summary>
        public int StatusCode { get; set; }
        /// <summary>
        /// Method used in request [GET, POST, PUT, etc.]
        /// </summary>
        public string Method { get; set; }
        /// <summary>
        /// Path of request
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Query string of request
        /// </summary>
        public string QueryString { get; set; }
        /// <summary>
        /// Content of request
        /// </summary>
        public string RequestBodyContent { get; set; }
        /// <summary>
        /// Content of response
        /// </summary>
        public string ResponseBodyContent { get; set; }
        #endregion
        #region Constructors
        public Log() { }
        public Log(DateTime RequestedAt, long RequestTime, int StatusCode, string Method, string Path, string RequestBodyContent, string ResponseBodyContent)
        {
            this.RequestedAt = RequestedAt;
            this.RequestTime = RequestTime;
            this.StatusCode = StatusCode;
            this.Method = Method;
            this.Path = Path;
            this.QueryString = QueryString;
            this.RequestBodyContent = RequestBodyContent;
            this.ResponseBodyContent = ResponseBodyContent;
        }
        #endregion
    }
}
