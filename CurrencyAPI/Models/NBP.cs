﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CurrencyAPI.Models
{
    /// <summary>
    /// Response from NBP Web Api with information about current exchange rates of currencies
    /// </summary>
    public class NBPTableResponse
    {
        /// <summary>
        /// Type of Table (A, B or C)
        /// </summary>
        public string Table { get; set; }
        /// <summary>
        /// Number of table
        /// </summary>
        public string No { get; set; }
        /// <summary>
        /// Publish date
        /// </summary>
        public DateTime EffectiveDate { get; set; }
        /// <summary>
        /// List of <see cref="MidCurrencyRate"/>
        /// </summary>
        public IEnumerable<MidCurrencyRate> Rates { get; set; }
    }

    /// <summary>
    /// Details about mid values of currency
    /// </summary>
    public class MidCurrencyRate
    {
        /// <summary>
        /// Full currency name
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Currency code in ISO 4217 format
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Mid exchange rate of currency
        /// </summary>
        public decimal Mid { get; set; }

        public MidCurrencyRate() { }

        public MidCurrencyRate(string Currency, string Code, decimal Mid)
        {
            this.Currency = Currency;
            this.Code = Code;
            this.Mid = Mid;
        }
    }

    /// <summary>
    /// All details of single currency
    /// </summary>
    public class SingleMidCurrencyRate
    {
        /// <summary>
        /// Type of Table (A, B or C)
        /// </summary>
        public string Table { get; set; }
        /// <summary>
        /// Full currency name
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Currency code in ISO 4217 format
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Array of current <see cref="RateDetail"/>
        /// </summary>
        public RateDetail[] Rates { get; set; }
        /// <summary>
        /// Actual <see cref="MidCurrencyRate"/> of currency
        /// </summary>
        public MidCurrencyRate Mid => new MidCurrencyRate(Currency, Code, Rates.FirstOrDefault().Mid);
    }

    /// <summary>
    /// Details of exchange rates for particular currency
    /// </summary>
    public class RateDetail
    {
        /// <summary>
        /// Number of table
        /// </summary>
        public string No { get; set; }
        /// <summary>
        /// Publish date
        /// </summary>
        public DateTime EffectiveDate { get; set; }
        /// <summary>
        /// Mid exchange rate for currency
        /// </summary>
        public decimal Mid { get; set; }
    }
}
