﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace CurrencyAPI.Models
{
    public interface ILogRepository
    {
        void SaveLog(DateTime RequestedAt, long RequestTime, int StatusCode, string Method, string Path, string RequestBodyContent, string ResponseBodyContent);
        IQueryable<Log> Logs { get; }
    }
}
