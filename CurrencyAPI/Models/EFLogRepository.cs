﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace CurrencyAPI.Models
{
    /// <summary>
    /// EntityFramework repository of <see cref="Log"/>
    /// </summary>
    public class EFLogRepository : ILogRepository
    {
        private readonly ApplicationDbContext _context;
        public EFLogRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public void SaveLog(DateTime RequestedAt, long RequestTime, int StatusCode, string Method, string Path, string RequestBodyContent, string ResponseBodyContent)
        {
            _context.Logs.Add(new Log(RequestedAt, RequestTime, StatusCode, Method, Path, RequestBodyContent, ResponseBodyContent));
            _context.SaveChanges();
        }
        public IQueryable<Log> Logs => _context.Logs;
    }
}
