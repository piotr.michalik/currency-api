﻿using System;
namespace CurrencyAPI.Models
{
    /// <summary>
    /// Result of currency exchange.
    /// </summary>
    public class ExchangeResult
    {
        #region Properties
        /// <summary>
        /// Code of source currency
        /// </summary>
        public string FromCurrencyCode { get; set; }
        /// <summary>
        /// Code of target currency (ISO 4217 format)
        /// </summary>
        public string ToCurrencyCode { get; set; }
        /// <summary>
        /// Amount of money in target currency (ISO 4217 format)
        /// </summary>
        public decimal ExchangeValue { get; set; }
        #endregion

        #region Constructors
        public ExchangeResult() { }

        public ExchangeResult(MidCurrencyRate fromCurrency, MidCurrencyRate toCurrency, decimal amountToExchange)
        {
            FromCurrencyCode = fromCurrency.Code;
            ToCurrencyCode = toCurrency.Code;
            ExchangeValue = Math.Round(fromCurrency.Mid * amountToExchange / toCurrency.Mid, 2);
        }
        #endregion
    }
}
