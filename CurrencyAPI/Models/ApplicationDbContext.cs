﻿using System;
using Microsoft.EntityFrameworkCore;

namespace CurrencyAPI.Models

{
    /// <summary>
    /// Application <see cref="DbContext"/> used by Entity Framework
    /// </summary>
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        /// <summary>
        /// Database set of <see cref="Log"/>
        /// </summary>
        public DbSet<Log> Logs { get; set; }
    }
}
