﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CurrencyAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CurrencyAPI.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class CurrencyController : Controller
    {
        private readonly Uri NBP_API_BASEURL = new Uri("http://api.nbp.pl/api/");
        private readonly ILogRepository _logRepository;

        public CurrencyController(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }

        /// <summary>
        /// Returns list of available currencies to exchange
        /// </summary>
        /// <returns><see cref="IEnumerable{MidCurrencyRate}"/></returns>
        /// <response code="200">Returns available rates</response>
        /// <response code="400">When NBP Web API is down</response>
        [HttpGet("get")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public ActionResult<IEnumerable<MidCurrencyRate>> Get()
        {
            HttpResponseMessage response = RequestNBP("exchangerates/tables/a");
            if (!response.IsSuccessStatusCode)
            {
                return BadRequest(response.ReasonPhrase);
            }
            NBPTableResponse result = JsonConvert.DeserializeObject<NBPTableResponse>(JsonConvert.DeserializeObject<JArray>(response.Content.ReadAsStringAsync().Result).First().ToString());
            return Ok(result.Rates);
        }

        /// <summary>
        /// List of actual exchange rates for specified codes of currencies
        /// </summary>
        /// <param name="codes">Codes of currencies in ISO 4217 format</param>
        /// <returns><see cref="IEnumerable{MidCurrencyRate}"/></returns>
        /// <response code="200">Returns current exchange rates of requested currencies</response>
        [HttpPost("get")]
        [ProducesResponseType(200)]
        public ActionResult<IEnumerable<MidCurrencyRate>> Get([Required][FromBody] List<string> codes)
        {
            List<MidCurrencyRate> result = new List<MidCurrencyRate>();
            foreach (var code in codes.Select(_ => _.ToLower().Trim()))
            {
                if (result.FirstOrDefault(_ => _.Code.ToLower() == code) != null) continue;
                HttpResponseMessage response = RequestNBP($"exchangerates/rates/a/{code}");
                if (response.IsSuccessStatusCode)
                {
                    result.Add(JsonConvert.DeserializeObject<SingleMidCurrencyRate>(response.Content.ReadAsStringAsync().Result).Mid);
                }
            }
            return Ok(result);
        }

        /// <summary>
        /// Exchanges one currency to other.
        /// </summary>
        /// <param name="fromCurrencyCode">Code of source currency (ISO 4217 format)</param>
        /// <param name="toCurrencyCode">Code of target currency (ISO 4217 format)</param>
        /// <param name="amount">Amount of source currency</param>
        /// <returns><see cref="ExchangeResult"/></returns>
        /// <response code="200">Returns <see cref="ExchangeResult"/> which holds information about exchange based on given parameters</response>
        /// <response code="400">When of given currency codes doesn't exists or parameters don't met their requirements</response>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [HttpGet("exchange/{fromCurrencyCode}/{toCurrencyCode}/{amount}")]
        public ActionResult<ExchangeResult> Exchange(
            [Required][RegularExpression(@"\b[a-zA-Z]{3}\b")]string fromCurrencyCode,
            [Required][RegularExpression(@"\b[a-zA-Z]{3}\b")]string toCurrencyCode,
            [Required][Range(0.01, double.MaxValue)]decimal amount)
        {
            if (ModelState.IsValid)
            {
                HttpResponseMessage fromCurrencyResponse = RequestNBP($"exchangerates/rates/a/{fromCurrencyCode}");
                HttpResponseMessage toCurrencyResponse = RequestNBP($"exchangerates/rates/a/{toCurrencyCode}");
                if (!fromCurrencyResponse.IsSuccessStatusCode || !toCurrencyResponse.IsSuccessStatusCode)
                {
                    return BadRequest();
                }
                SingleMidCurrencyRate fromCurrency = JsonConvert.DeserializeObject<SingleMidCurrencyRate>(fromCurrencyResponse.Content.ReadAsStringAsync().Result);
                SingleMidCurrencyRate toCurrency = JsonConvert.DeserializeObject<SingleMidCurrencyRate>(toCurrencyResponse.Content.ReadAsStringAsync().Result);
                return new ExchangeResult(fromCurrency.Mid, toCurrency.Mid, amount);
            }
            return BadRequest(ModelStateErrorMessage(ModelState.Values.SelectMany(v => v.Errors)));
        }

        /// <summary>
        /// Requests NBP Web Api to achieve required information.
        /// </summary>
        /// <param name="parameters">Parameters added at the end of <see cref="NBP_API_BASEURL"/></param>
        /// <returns><see cref="HttpResponseMessage"/></returns>
        private HttpResponseMessage RequestNBP(string parameters)
        {
            HttpClient client = new HttpClient
            {
                BaseAddress = NBP_API_BASEURL
            };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            DateTime requestTime = DateTime.UtcNow;
            Stopwatch stopwatch = Stopwatch.StartNew();
            string path = $"{parameters}?format=json";
            HttpResponseMessage result = client.GetAsync(path).Result;
            stopwatch.Stop();
            _logRepository.SaveLog(requestTime, stopwatch.ElapsedMilliseconds, (int)result.StatusCode, "GET", $"{client.BaseAddress}{path}", null, null);
            client.Dispose();
            return result;
        }

        /// <summary>
        /// Prints errors from model state into a string
        /// </summary>
        /// <param name="errors">List of <see cref="ModelError"/> delivered by <see cref="ModelStateDictionary"/></param>
        /// <returns></returns>
        private string ModelStateErrorMessage(IEnumerable<ModelError> errors) => $"Wrong parameters. Error list: {string.Join(',', errors)}";
    }
}
